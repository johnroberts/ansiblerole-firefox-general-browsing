# Summary
This role installs and configures Firefox ESR with an opinionated general-purpose browsing config and extensions on Debian/MX Linux. It balances privacy, security, and usability.

Features:
- Curated config via `user.js`, inheriting from https://github.com/yokoffing/Betterfox
- Curated extensions via `policies.json` ExtensionSettings (https://mozilla.github.io/policy-templates/#extensionsettings)
- Disable Firefox annoyances such as telemetry, sponsored content, pocket, account sync, password manager (prefer Bitwarden), save credit card/addresses, and more
- DuckDuckGo as default search engine

**WARNING**: this role deletes existing profiles and takes exclusive profile management for the host and user where it runs. Make sure this is what you want, don't lose your Firefox profile/data/bookmarks/etc.!

# Applied customizations
- Base firefox config, extensions -> [templates/policies.json.j2](templates/policies.json.j2)
User-level config based on Betterfox: `files/betterfox-with-overrides-user.js`

# Usage
Read [defaults/main.yml](defaults/main.yml), override vars as needed when calling the role

# Platforms
Tested against:
- MX Linux 23 (based on Debian stable)

Probably works without modification on other Debian-compatible distros. May work (possibly with modifications) on other Debian or Ubuntu variants.

# Internal Workings
- Takes exclusive control of firefox profile for the system and user it runs as
- Custom `user.js` based on Betterfox
- Custom `policies.json`, based on my own research

# Stuff to keep up-to-date
- `files/betterfox-with-overrides-user.js`, whenever https://raw.githubusercontent.com/yokoffing/Betterfox/main/user.js changes, re-add overrides